﻿using System;

namespace tp02_console
{
    public class Program
    {
        public static Boolean startWithMaj(string phrase) {
            int position = 0;
            char[] majuscule = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
            while (phrase[position] == ' ') position++;
            foreach (char lettre in majuscule)
            {
                if (lettre == phrase[0]) return true;
            }
            return false;
        }
        public static Boolean endWithPoint(string phrase) {
            int position = phrase.Length - 1;
            char lastChar = phrase[position];
            while (lastChar == ' ') {lastChar = phrase[position-1]; position--;}
            if (lastChar == '.') return true;
            else return false;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
