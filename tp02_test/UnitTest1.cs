using System;
using tp02_console;
using Xunit;

namespace tp02_test
{
    public class UnitTest1
    {
        [Fact]
        public void testMaj()
        {
            Assert.Equal(true, Program.startWithMaj("Majuscule"));
            Assert.Equal(false, Program.startWithMaj("minuscule"));
            Assert.Equal(false, Program.startWithMaj("123"));
            Assert.Equal(false, Program.startWithMaj("@_L"));
            Assert.Equal(true, Program.startWithMaj(" Majuscule avec espace avant"));
            Assert.Equal(false, Program.startWithMaj("pANKAKE"));
        }
        [Fact]
        public void testPoint()
        {
            Assert.Equal(true, Program.endWithPoint("Phrase."));
            Assert.Equal(false, Program.endWithPoint("Pas phrase"));
            Assert.Equal(true, Program.endWithPoint("Phrase avec beaucoup d'espaces après.          "));
            Assert.Equal(false, Program.endWithPoint("\\jdzkoi"));
            Assert.Equal(true, Program.endWithPoint("."));
            Assert.Equal(false, Program.endWithPoint(";;!.;"));
            Assert.Equal(false, Program.endWithPoint(" "));
        }
    }
}
